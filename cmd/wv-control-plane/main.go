package main

import (
	"flag"
	"gitlab.com/amazing-unicorns/wv-control-plane.git/internal/data"
	"gitlab.com/amazing-unicorns/wv-control-plane.git/internal/pkg/temporal"
	"go.temporal.io/sdk/worker"
	"os"

	"gitlab.com/amazing-unicorns/wv-control-plane.git/internal/conf"

	"github.com/go-kratos/kratos/v2"
	"github.com/go-kratos/kratos/v2/config"
	"github.com/go-kratos/kratos/v2/config/file"
	"github.com/go-kratos/kratos/v2/log"
	"github.com/go-kratos/kratos/v2/middleware/tracing"
	"github.com/go-kratos/kratos/v2/transport/grpc"
	"github.com/go-kratos/kratos/v2/transport/http"

	_ "go.uber.org/automaxprocs"
)

// go build -ldflags "-X main.Version=x.y.z"
var (
	// Name is the name of the compiled software.
	Name string
	// Version is the version of the compiled software.
	Version string
	// flagconf is the config flag.
	flagconf string

	id, _ = os.Hostname()
)

func init() {
	flag.StringVar(&flagconf, "conf", "./configs", "config path, eg: -conf config.yaml")
}

func newApp(logger log.Logger, gs *grpc.Server, hs *http.Server) *kratos.App {
	return kratos.New(
		kratos.ID(id),
		kratos.Name(Name),
		kratos.Version(Version),
		kratos.Metadata(map[string]string{}),
		kratos.Logger(logger),
		kratos.Server(
			gs,
			hs,
		),
	)
}

func main() {
	flag.Parse()
	logger := log.With(log.NewStdLogger(os.Stdout),
		"ts", log.DefaultTimestamp,
		"caller", log.DefaultCaller,
		"trace.id", tracing.TraceID(),
		"span.id", tracing.SpanID(),
	)
	c := config.New(
		config.WithSource(
			file.NewSource(flagconf),
		),
	)
	defer c.Close()

	if err := c.Load(); err != nil {
		panic(err)
	}

	var bc conf.Bootstrap
	if err := c.Scan(&bc); err != nil {
		panic(err)
	}

	app, cleanup, err := wireApp(bc.Server, bc.Data, logger)
	if err != nil {
		panic(err)
	}
	defer cleanup()

	go func() {
		// Create a new worker listening on the stick queue
		dbWorker := worker.New(data.NewTemporalClient(bc.Data), "wv_control_plane", worker.Options{})

		a := &temporal.WeaviateActivities{
			Db:   data.NewTiDbClient(),
			Kube: data.NewKubeClient(),
		}

		dbWorker.RegisterWorkflow(temporal.ProvisionWeaviate)
		dbWorker.RegisterActivity(a)

		err = dbWorker.Run(worker.InterruptCh())
		if err != nil {
			panic(err)
		}
	}()

	// start and wait for stop signal
	if err := app.Run(); err != nil {
		panic(err)
	}
}

// TODO: intialize otel tracer
