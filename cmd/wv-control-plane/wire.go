//go:build wireinject
// +build wireinject

// The build tag makes sure the stub is not built in the final build.

package main

import (
	"gitlab.com/amazing-unicorns/wv-control-plane.git/internal/biz"
	"gitlab.com/amazing-unicorns/wv-control-plane.git/internal/conf"
	"gitlab.com/amazing-unicorns/wv-control-plane.git/internal/data"
	"gitlab.com/amazing-unicorns/wv-control-plane.git/internal/server"
	"gitlab.com/amazing-unicorns/wv-control-plane.git/internal/service"

	"github.com/go-kratos/kratos/v2"
	"github.com/go-kratos/kratos/v2/log"
	"github.com/google/wire"
)

// wireApp init kratos application.
func wireApp(*conf.Server, *conf.Data, log.Logger) (*kratos.App, func(), error) {
	panic(wire.Build(server.ProviderSet, data.ProviderSet, biz.ProviderSet, service.ProviderSet, newApp))
}
