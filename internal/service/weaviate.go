package service

import (
	"context"
	"github.com/go-kratos/kratos/v2/log"
	wvpb "gitlab.com/amazing-unicorns/wv-control-plane.git/api/weaviate/v1"
	"gitlab.com/amazing-unicorns/wv-control-plane.git/internal/biz"
	"gitlab.com/amazing-unicorns/wv-control-plane.git/internal/pkg/mappers"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/trace"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// WeaviateService is a weaviate service.
type WeaviateService struct {
	wvpb.UnimplementedWeaviateServer

	uc     *biz.WeaviateUsecase
	tracer trace.Tracer
	log    *log.Helper
}

// NewWeaviateService new a greeter service.
func NewWeaviateService(uc *biz.WeaviateUsecase, logger log.Logger) *WeaviateService {
	return &WeaviateService{
		uc:     uc,
		log:    log.NewHelper(logger),
		tracer: otel.Tracer("api")}
}

func (w *WeaviateService) CreateWeaviate(ctx context.Context, req *wvpb.CreateWeaviateRequest) (*wvpb.CreateWeaviateResponse, error) {
	ctx, span := w.tracer.Start(ctx, "CreateWeaviate")
	defer span.End()

	wv, err := w.uc.Create(ctx, req)
	if err != nil {
		w.log.Errorf("failed to create weaviate instance %v", err)
		return nil, err
	}

	w.log.Infow(
		"msg", "created task to provision weaviate",
		"instance", wv,
	)

	return &wvpb.CreateWeaviateResponse{
		Weaviate: &wvpb.WeaviateDb{
			Id:         int32(wv.ID),
			CustomerId: wv.CustomerID.String(),
			Plan:       mappers.MapPlan(wv.Plan),
			Replicas:   int32(wv.Replicas),
			Status:     wv.Status.String(),
			CreatedAt:  timestamppb.New(wv.CreatedAt),
		},
	}, nil
}

func (w *WeaviateService) GetWeaviate(ctx context.Context, req *wvpb.GetWeaviateRequest) (*wvpb.GetWeaviateResponse, error) {
	ctx, span := w.tracer.Start(ctx, "GetWeaviate")
	defer span.End()

	wv, err := w.uc.Get(ctx, int(req.Id))
	if err != nil {
		w.log.Errorf("failed to retrieve weaviate instance %v", err)
		return nil, err
	}

	w.log.Infow(
		"msg", "retrieved weaviate instance",
		"instance", wv,
	)

	return &wvpb.GetWeaviateResponse{Weaviate: &wvpb.WeaviateDb{
		Id:         int32(wv.ID),
		CustomerId: wv.CustomerID.String(),
		Plan:       mappers.MapPlan(wv.Plan),
		Replicas:   int32(wv.Replicas),
		Status:     wv.Status.String(),
		CreatedAt:  timestamppb.New(wv.CreatedAt),
	}}, nil
}

func (w *WeaviateService) ListWeaviate(ctx context.Context, req *wvpb.ListWeaviateRequest) (*wvpb.ListWeaviateResponse, error) {
	ctx, span := w.tracer.Start(ctx, "ListWeaviate")
	defer span.End()

	reply, err := w.uc.List(ctx, req)
	if err != nil {
		w.log.Errorf("failed to retrieve weaviate instances %v", err)
		return nil, err
	}

	w.log.Info("retrieved weaviate instances")

	return reply, nil
}

func (w *WeaviateService) DeleteWeaviate(ctx context.Context, req *wvpb.DeleteWeaviateRequest) (*wvpb.DeleteWeaviateResponse, error) {
	ctx, span := w.tracer.Start(ctx, "DeleteWeaviate")
	defer span.End()

	err := w.uc.Delete(ctx, int(req.Id))
	if err != nil {
		w.log.Errorf("failed to delete weaviate instance %v", err)
		return nil, err
	}

	w.log.Infow(
		"msg", "removed weaviate instance",
		"instance_id", req.Id,
	)

	return &wvpb.DeleteWeaviateResponse{}, nil
}
