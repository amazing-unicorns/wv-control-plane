package data

import (
	"context"
	"fmt"
	"github.com/docker/docker/pkg/namesgenerator"
	"github.com/go-kratos/kratos/v2/log"
	"github.com/google/uuid"
	"gitlab.com/amazing-unicorns/wv-control-plane.git/internal/pkg/temporal"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/trace"
	"go.temporal.io/sdk/client"
	temporal_internal "go.temporal.io/sdk/temporal"
	"strings"

	wvpb "gitlab.com/amazing-unicorns/wv-control-plane.git/api/weaviate/v1"
	"gitlab.com/amazing-unicorns/wv-control-plane.git/internal/biz"
	"gitlab.com/amazing-unicorns/wv-control-plane.git/internal/data/ent"
	"gitlab.com/amazing-unicorns/wv-control-plane.git/internal/data/ent/weaviate"
	"gitlab.com/amazing-unicorns/wv-control-plane.git/internal/pkg/mappers"
	"time"
)

type weaviateRepo struct {
	data   *Data
	log    *log.Helper
	tracer trace.Tracer
}

// NewWeaviateRepo .
func NewWeaviateRepo(data *Data, logger log.Logger) biz.WeaviateRepo {
	return &weaviateRepo{
		data:   data,
		log:    log.NewHelper(logger),
		tracer: otel.Tracer("data"),
	}
}

// Create insert record into db about new instance
// and triggers provision workflow
func (w *weaviateRepo) Create(ctx context.Context, req *wvpb.CreateWeaviateRequest) (*ent.Weaviate, error) {
	ctx, span := w.tracer.Start(ctx, "Create")
	defer span.End()

	// parse plan from pb to ent structure
	plan, err := mappers.ParsePlan(req.Plan)
	if err != nil {
		return nil, err
	}

	// generate random name for db instance
	instanceName := strings.ReplaceAll(namesgenerator.GetRandomName(0), "_", "-")

	tx, err := w.data.db.Tx(ctx)
	if err != nil {
		return nil, err
	}

	// create record into db
	wv, err := tx.Weaviate.
		Create().
		SetInstanceName(instanceName).
		SetCustomerID(uuid.MustParse(req.CustomerId)).
		SetPlan(plan).
		SetReplicas(int(req.Replicas)).
		SetStatus(weaviate.StatusPending).
		Save(ctx)
	if err != nil {
		return nil, err
	}

	// schedule provision workflow
	we, err := w.provision(ctx, wv.ID)
	if err != nil {
		tx.Rollback()
		return nil, err
	}

	// link workflow with db record
	wv, err = tx.Weaviate.
		UpdateOneID(wv.ID).
		SetLinkedWorkflowID(we.GetID()).
		SetLinkedWorkflowRunID(we.GetRunID()).
		Save(ctx)
	if err != nil {
		tx.Rollback()
		return nil, err
	}

	err = tx.Commit()
	if err != nil {
		return nil, err
	}

	return wv, nil
}

func (w *weaviateRepo) provision(ctx context.Context, id int) (client.WorkflowRun, error) {
	ctx, span := w.tracer.Start(ctx, "provision")
	defer span.End()

	// define workflow options, retry policy
	workflowOptions := client.StartWorkflowOptions{
		ID:        fmt.Sprintf("provision-weaviate-%d", id),
		TaskQueue: "wv_control_plane",
		RetryPolicy: &temporal_internal.RetryPolicy{
			InitialInterval:    time.Second,
			BackoffCoefficient: 2.0,
			MaximumInterval:    time.Second * 100,
			MaximumAttempts:    5,
		},
	}

	// trigger workflow execution
	we, err := w.data.tctl.ExecuteWorkflow(
		ctx,
		workflowOptions,
		temporal.ProvisionWeaviate,
		id,
	)
	if err != nil {
		return nil, err
	}

	return we, nil
}

func (w *weaviateRepo) Get(ctx context.Context, id int) (*ent.Weaviate, error) {
	ctx, span := w.tracer.Start(ctx, "Get")
	defer span.End()

	wv, err := w.data.db.Weaviate.Get(ctx, id)
	if err != nil {
		return nil, err
	}

	return wv, nil
}

func (w *weaviateRepo) List(ctx context.Context, req *wvpb.ListWeaviateRequest) (*wvpb.ListWeaviateResponse, error) {
	ctx, span := w.tracer.Start(ctx, "List")
	defer span.End()

	wvs, err := w.data.db.Weaviate.
		Query().
		All(ctx)
	if err != nil {
		return nil, err
	}

	// convert to protobuf
	wvsPb := make([]*wvpb.WeaviateDb, 0, len(wvs))
	for _, wv := range wvs {

		wvPb := mappers.MapWeaviate(wv)
		wvsPb = append(wvsPb, wvPb)

	}

	// TODO: cursor pagination support

	return &wvpb.ListWeaviateResponse{Weaviates: wvsPb}, nil
}

func (w *weaviateRepo) Delete(ctx context.Context, id int) error {
	ctx, span := w.tracer.Start(ctx, "Delete")
	defer span.End()

	err := w.data.db.Weaviate.
		DeleteOneID(id).
		Exec(ctx)
	if err != nil {
		return err
	}

	// TODO: implement delete workflow

	return nil
}
