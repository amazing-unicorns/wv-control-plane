package data

import (
	"context"
	dbv1alpha1 "github.com/brodyaga777/weaviate-operator/api/v1alpha1"
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/amazing-unicorns/wv-control-plane.git/internal/conf"
	"gitlab.com/amazing-unicorns/wv-control-plane.git/internal/data/ent"
	"gitlab.com/amazing-unicorns/wv-control-plane.git/internal/data/ent/migrate"
	tctl "go.temporal.io/sdk/client"
	"k8s.io/apimachinery/pkg/runtime"
	"os"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/config"

	"github.com/go-kratos/kratos/v2/log"
	"github.com/google/wire"
)

// ProviderSet is data providers.
var ProviderSet = wire.NewSet(
	NewData,
	NewWeaviateRepo,
	NewTiDbClient,
	NewTemporalClient,
)

// Data .
type Data struct {
	db   *ent.Client
	tctl tctl.Client
}

// NewData .
func NewData(logger log.Logger, db *ent.Client, temporal tctl.Client) (*Data, func(), error) {
	// cleanup handler
	cleanup := func() {
		log.NewHelper(logger).Info("closing the data resources")
		db.Close()
		temporal.Close()
	}
	return &Data{
		db:   db,
		tctl: temporal,
	}, cleanup, nil
}

// NewTiDbClient create connection to tidb instance
// learn more: https://docs.pingcap.com/tidb/stable/dev-guide-sample-application-golang-sql-driver/
func NewTiDbClient() *ent.Client {
	// open connection
	// TODO: use vault
	db, err := ent.Open("mysql", "root@tcp(localhost:4000)/wv_control_plane?parseTime=true")
	if err != nil {
		panic(err)
	}

	// run auto migration
	err = db.Schema.Create(context.Background(), migrate.WithForeignKeys(false))
	if err != nil {
		panic(err)
	}

	return db
}

// NewTemporalClient creates connection to temporal-frontend service
func NewTemporalClient(conf *conf.Data) tctl.Client {
	tctlHost := os.Getenv("TEMPORAL_FRONTEND_URL")

	// create temporal client
	client, err := tctl.Dial(tctl.Options{HostPort: tctlHost})
	if err != nil {
		panic(err)
	}

	return client
}

// NewKubeClient creates new kubernetes client
// with support dbv1alpha1.Weaviate added to scheme
// https://github.com/kubernetes-sigs/kubebuilder/issues/1152
func NewKubeClient() client.Client {
	scheme := runtime.NewScheme()
	if err := dbv1alpha1.AddToScheme(scheme); err != nil {
		panic(err)
	}

	// will extract $HOME/.kube/config if exists.
	kubeConfig, err := config.GetConfig()
	if err != nil {
		panic(err)
	}

	kubeClient, err := client.New(kubeConfig, client.Options{Scheme: scheme})
	if err != nil {
		panic(err)
	}

	return kubeClient
}
