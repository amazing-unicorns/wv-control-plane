// Code generated by ent, DO NOT EDIT.

package migrate

import (
	"entgo.io/ent/dialect/sql/schema"
	"entgo.io/ent/schema/field"
)

var (
	// WeaviatesColumns holds the columns for the "weaviates" table.
	WeaviatesColumns = []*schema.Column{
		{Name: "id", Type: field.TypeInt, Increment: true},
		{Name: "instance_name", Type: field.TypeString},
		{Name: "customer_id", Type: field.TypeUUID},
		{Name: "plan", Type: field.TypeEnum, Enums: []string{"starter", "scale", "super_duper"}},
		{Name: "replicas", Type: field.TypeInt},
		{Name: "status", Type: field.TypeEnum, Enums: []string{"pending", "provisioning", "provisioned", "running", "failed"}, Default: "pending"},
		{Name: "created_at", Type: field.TypeTime},
		{Name: "linked_workflow_id", Type: field.TypeString, Nullable: true},
		{Name: "linked_workflow_run_id", Type: field.TypeString, Nullable: true},
	}
	// WeaviatesTable holds the schema information for the "weaviates" table.
	WeaviatesTable = &schema.Table{
		Name:       "weaviates",
		Columns:    WeaviatesColumns,
		PrimaryKey: []*schema.Column{WeaviatesColumns[0]},
	}
	// Tables holds all the tables in the schema.
	Tables = []*schema.Table{
		WeaviatesTable,
	}
)

func init() {
}
