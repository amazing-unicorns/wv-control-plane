package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/field"
	"github.com/google/uuid"
	"time"
)

// Weaviate holds the schema definition for the Weaviate entity.
type Weaviate struct {
	ent.Schema
}

// Fields of the Weaviate.
func (Weaviate) Fields() []ent.Field {
	return []ent.Field{
		field.String("instance_name"),
		field.UUID("customer_id", uuid.UUID{}),
		field.Enum("plan").
			Values("starter", "scale", "super_duper"),
		field.Int("replicas"),
		field.Enum("status").
			Values("pending", "provisioning", "provisioned", "running", "failed").
			Default("pending"),
		field.Time("created_at").Default(time.Now),
		field.String("linked_workflow_id").Optional(),
		field.String("linked_workflow_run_id").Optional(),
	}
}

// Edges of the Weaviate.
func (Weaviate) Edges() []ent.Edge {
	return nil
}
