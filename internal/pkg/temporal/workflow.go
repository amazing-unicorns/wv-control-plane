package temporal

import (
	dbv1alpha1 "github.com/brodyaga777/weaviate-operator/api/v1alpha1"
	"gitlab.com/amazing-unicorns/wv-control-plane.git/internal/data/ent"
	entweaviate "gitlab.com/amazing-unicorns/wv-control-plane.git/internal/data/ent/weaviate"
	"go.temporal.io/sdk/temporal"
	"go.temporal.io/sdk/workflow"
	"time"
)

// TODO: implement monitoring workflow

// ProvisionWeaviate TODO: implement saga pattern with rollbacks
func ProvisionWeaviate(ctx workflow.Context, id int) error {
	ao := workflow.ActivityOptions{
		StartToCloseTimeout: 100 * time.Second,
		RetryPolicy: &temporal.RetryPolicy{
			InitialInterval:    time.Second * 10,
			BackoffCoefficient: 3.0,
			MaximumInterval:    time.Second * 10,
			MaximumAttempts:    50,
		},
	}
	ctx = workflow.WithActivityOptions(ctx, ao)

	var (
		a  *WeaviateActivities
		wv *ent.Weaviate
	)

	// 1. retrieve weaviate instance from db
	err := workflow.ExecuteActivity(ctx, a.GetWeaviateInstance, id).Get(ctx, &wv)
	if err != nil {
		return err
	}

	// 2. create weaviate instance into kubernetes
	err = workflow.ExecuteActivity(
		ctx,
		a.CreateWeaviateInstance,
		wv,
	).Get(ctx, nil)
	if err != nil {
		return err
	}

	for {
		// 3. retrieve k8s instance to check it status

		var db dbv1alpha1.Weaviate
		err = workflow.ExecuteActivity(ctx, a.GetWeaviateInstanceK8s, wv.InstanceName).Get(ctx, &db)
		if err != nil {
			return err
		}

		// instance is provisioned, so break
		if db.Status.Ready {
			break
		}

		_ = workflow.Sleep(ctx, time.Second*30)
	}

	// 4. mark as provisioned
	wv.Status = entweaviate.StatusProvisioned
	err = workflow.ExecuteActivity(ctx, a.UpdateWeaviateInstanceStatus, wv).Get(ctx, nil)
	if err != nil {
		return err
	}

	return nil
}
