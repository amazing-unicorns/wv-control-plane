package temporal

import (
	"context"
	dbv1alpha1 "github.com/brodyaga777/weaviate-operator/api/v1alpha1"
	"gitlab.com/amazing-unicorns/wv-control-plane.git/internal/data/ent"
	"go.temporal.io/sdk/activity"
	"k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type WeaviateActivities struct {
	Db   *ent.Client
	Kube client.Client
}

func (w *WeaviateActivities) CreateWeaviateInstance(ctx context.Context, wv *ent.Weaviate) error {

	db := dbv1alpha1.Weaviate{
		TypeMeta: v1.TypeMeta{
			Kind:       "Weavite",
			APIVersion: "db.weaviate.io/v1alpha1",
		},
		ObjectMeta: v1.ObjectMeta{
			Name:      wv.InstanceName,
			Namespace: "default",
		},
		Spec: dbv1alpha1.WeaviateSpec{Replicas: int32(wv.Replicas)},
	}

	err := w.Kube.Create(ctx, &db)
	if err != nil {
		return err
	}

	return nil
}

func (w *WeaviateActivities) GetWeaviateInstance(ctx context.Context, id int) (*ent.Weaviate, error) {
	return w.Db.Weaviate.Get(ctx, id)
}

func (w *WeaviateActivities) GetWeaviateInstanceK8s(ctx context.Context, name string) (dbv1alpha1.Weaviate, error) {
	logger := activity.GetLogger(ctx)

	var db dbv1alpha1.Weaviate
	err := w.Kube.Get(context.Background(), client.ObjectKey{
		Namespace: "default",
		Name:      name,
	}, &db)
	if err != nil {
		logger.Error("holy fuck")
	}

	return db, err
}

func (w *WeaviateActivities) DeleteWeaviateInstance(ctx context.Context) (*ent.Weaviate, error) {
	return nil, nil
}

func (w *WeaviateActivities) UpdateWeaviateInstanceStatus(ctx context.Context, wv *ent.Weaviate) error {

	_, err := w.Db.Weaviate.
		UpdateOneID(wv.ID).
		SetStatus(wv.Status).
		Save(ctx)
	if err != nil {
		return err
	}

	return nil
}
