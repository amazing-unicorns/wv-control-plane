package mappers

import (
	"errors"
	wvpb "gitlab.com/amazing-unicorns/wv-control-plane.git/api/weaviate/v1"
	"gitlab.com/amazing-unicorns/wv-control-plane.git/internal/data/ent/weaviate"
)

func ParsePlan(plan wvpb.Plan) (weaviate.Plan, error) {

	switch plan {
	case wvpb.Plan_STARTER:
		return weaviate.PlanStarter, nil
	case wvpb.Plan_SCALE:
		return weaviate.PlanScale, nil
	case wvpb.Plan_SUPER_DUPER:
		return weaviate.PlanSuperDuper, nil
	default:
		return weaviate.PlanStarter, errors.New("unsupported plan")
	}

}

func MapPlan(plan weaviate.Plan) wvpb.Plan {
	switch plan {
	case weaviate.PlanStarter:
		return wvpb.Plan_STARTER
	case weaviate.PlanScale:
		return wvpb.Plan_SCALE
	case weaviate.PlanSuperDuper:
		return wvpb.Plan_SUPER_DUPER
	default:
		return wvpb.Plan_UNSPECIFIED
	}
}
