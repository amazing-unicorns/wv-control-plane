package mappers

import (
	wvpb "gitlab.com/amazing-unicorns/wv-control-plane.git/api/weaviate/v1"
	"gitlab.com/amazing-unicorns/wv-control-plane.git/internal/data/ent"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func MapWeaviate(wv *ent.Weaviate) *wvpb.WeaviateDb {
	return &wvpb.WeaviateDb{
		Id:         int32(wv.ID),
		CustomerId: wv.CustomerID.String(),
		Plan:       MapPlan(wv.Plan),
		Replicas:   int32(wv.Replicas),
		Status:     wv.Status.String(),
		CreatedAt:  timestamppb.New(wv.CreatedAt),
	}
}
