package biz

import (
	"context"
	"github.com/go-kratos/kratos/v2/log"
	wvpb "gitlab.com/amazing-unicorns/wv-control-plane.git/api/weaviate/v1"
	"gitlab.com/amazing-unicorns/wv-control-plane.git/internal/data/ent"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/trace"
)

// WeaviateRepo is a Weaviate repo
type WeaviateRepo interface {
	Create(ctx context.Context, req *wvpb.CreateWeaviateRequest) (*ent.Weaviate, error)
	Get(ctx context.Context, id int) (*ent.Weaviate, error)
	List(ctx context.Context, req *wvpb.ListWeaviateRequest) (*wvpb.ListWeaviateResponse, error)
	Delete(ctx context.Context, id int) error
}

// WeaviateUsecase is a Greeter usecase.
type WeaviateUsecase struct {
	repo   WeaviateRepo
	log    *log.Helper
	tracer trace.Tracer
}

// NewWeaviateUsecaseUsecase new a Greeter usecase.
func NewWeaviateUsecaseUsecase(repo WeaviateRepo, logger log.Logger) *WeaviateUsecase {
	return &WeaviateUsecase{
		repo:   repo,
		log:    log.NewHelper(logger),
		tracer: otel.Tracer("biz"),
	}
}

func (w *WeaviateUsecase) Create(ctx context.Context, req *wvpb.CreateWeaviateRequest) (*ent.Weaviate, error) {
	ctx, span := w.tracer.Start(ctx, "Create")
	defer span.End()

	return w.repo.Create(ctx, req)
}

func (w *WeaviateUsecase) Get(ctx context.Context, id int) (*ent.Weaviate, error) {
	ctx, span := w.tracer.Start(ctx, "Get")
	defer span.End()

	return w.repo.Get(ctx, id)
}

func (w *WeaviateUsecase) List(ctx context.Context, req *wvpb.ListWeaviateRequest) (*wvpb.ListWeaviateResponse, error) {
	ctx, span := w.tracer.Start(ctx, "List")
	defer span.End()

	return w.repo.List(ctx, req)
}

func (w *WeaviateUsecase) Delete(ctx context.Context, id int) error {
	ctx, span := w.tracer.Start(ctx, "Delete")
	defer span.End()

	return w.repo.Delete(ctx, id)
}
